#!/bin/bash

set -ex

npm config set scripts-prepend-node-path true
npm config set node_gyp

yarn monaco-compile-check
yarn valid-layers-check

# The next 3 tasks can be replaced by `gulp core-ci` if we are OK
# with running an extra `minify-vscode-reh-web` task
yarn gulp compile-build
yarn gulp minify-vscode
yarn gulp minify-vscode-reh

# The extension tasks are necessary
# Without them the server doens't start because it's missing extensions
yarn gulp compile-extension-media
yarn gulp compile-extensions-build

declare -a os_and_archs=(
	"darwin-x64"
	"darwin-arm64"
	"win32-x64"
	"linux-x64"
	"linux-arm64"
)

for os_and_arch in "${os_and_archs[@]}"
do
	yarn gulp vscode-reh-${os_and_arch}-min-ci

	BUNDLE_NAME=vscode-reh-${os_and_arch}-$(cat VERSION)-dev-$(cat TIMESTAMP)
	mkdir -p .build/vscode-reh
	tar -czvf .build/vscode-reh/${BUNDLE_NAME}.tar.gz -C ../vscode-reh-${os_and_arch} .
done
