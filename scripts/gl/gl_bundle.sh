#!/usr/bin/env bash

BUNDLE_NAME=vscode-web-$(cat VERSION)-dev-$(cat TIMESTAMP)

mkdir .build/vscode-web-dist
tar -czvf .build/vscode-web-dist/${BUNDLE_NAME}.tar.gz -C .build/vscode-web .
