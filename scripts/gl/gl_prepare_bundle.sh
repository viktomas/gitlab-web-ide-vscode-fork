#!/usr/bin/env bash

rm -rf .build/vscode-web
mkdir .build/vscode-web

# Step. Move the compiled VSCode to the build directory
cp -r out-vscode-web-min .build/vscode-web/out

# Step. Move the compiled built-in extensions to the build directory
cp -r .build/web/extensions .build/vscode-web/extensions

# Step. Move the runtime web node_modules to the build directory
cp -r remote/web/node_modules .build/vscode-web
cp remote/web/package.json .build/vscode-web
