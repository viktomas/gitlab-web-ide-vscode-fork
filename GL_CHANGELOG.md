# GitLab fork Changelog

This documents a list of cohesive changes made for the GitLab fork of [vscode](https://github.com/microsoft/vscode).

## 1.69.1-1.0.0

- Fork initialization and documentation.
  - https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/12
  - Initialized this fork ontop of [upstream tag 1.69.1](https://github.com/microsoft/vscode/tree/1.69.1)
  - Added files:
    - `GL_CHANGELOG.md`
    - `GL_CONTRIBUTING.md`
    - `GL_FORK_PROCESS.md`
  - Updated `README.md`
  - Updated `.vscode/settings.json`
    - We don't need `alwaysCommitToNewBranch` and branch protections..
  - Added `.gitlab-ci.yml` to create build artifacts
  - Added `scripts/gl/gl_prepare_bundle.sh` and `scripts/gl/gl_bundle.sh` to help facilitate building the final build artifact
- Add hook into `BuiltinExtensionsScannerService` which will read from `gitlab-builtin-vscode-extensions` element a collection of `IBundledExtension`
  to append to it's known list.
- Updated `build/npm/postinstall.js` to disable some `git config`.
  - For some reason we were running into a `fatal: not in a git directory` error. https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/jobs/2774034213
- Added `gitlab:build-vscode-web` yarn task for development purposes. This can be used to run the whole build pipeline for `.build/vscode-web`.
  - Also, this supports https://gitlab.com/gitlab-org/gitlab-web-ide/-/merge_requests/27
