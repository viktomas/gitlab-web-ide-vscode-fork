# Procedures

## Version format

Here's an example of a version for the artifacts deployed from this project:

```
1.69.1-0.0.1-dev-20220722150450
```

- `1.69.1` is the base VSCode version
- `0.0.1` is the version of this fork
- `dev` is a flag that the release is meant only for development purposes.
  This could be replaced with `rc` if the artifact was a release candidate.
- `20220722150450` is the timestamp of the artifacts creation

We'll use GitLab releases to host these release artifacts and create tags based
on the version name of the artifact created in the `main` branch.

**Please note:** Official releases will not have `rc`, `dev`, or timestamp extension.

## Creating release

1.  Ensure pipleine on `main` is passing.
2.  Upload the `main` package to the project's generic Package Registry.

    1.  Download the `vscode-dist/...tar.gz` file from the `build-and-bundle` job artifacts of the passing pipeline.
    1.  The version will be part of the `...tar.gz` file name.
        For instance use `1.71.1-1.0.0-dev-20220915124437` if the job artifact generated was called `vscode-web-1.71.1-1.0.0-dev-20220915124437.tar.gz`. If creating a `rc` release, replace `dev` with `rc`. If creating an official release, drop the `-dev-...` altogether.
    1.  Run:

        ```
        curl --header "PRIVATE-TOKEN: <access_token>" \
              --upload-file path/to/...tar.gz \
              https://gitlab.com/api/v4/projects/37966217/packages/generic/gitlab-web-ide-vscode-fork/<version>/<file_name>.tar.gz
        ```

3.  Create a tag through GitLab's UI (**Repository** > **Tags** > **New tag**).
    1. For **Create from**, use `main`.
    1. For the **Name**, use the version from the `build-and-bundle` job artifacts of the passing pipeline. For instance, if creating a tag for a "development" release, use `1.71.1-1.0.0-dev-20220915124437` if the job artifact generated was called `vscode-web-1.71.1-1.0.0-dev-20220915124437.tar.gz`. If creating a `rc` release, replace `dev` with `rc`. If creating an official release, drop the `-dev-...` altogether.
    1. For the **Message**, consider including any URL's for traceability and context.
4.  Create a release through GitLab's UI (**Deployments** > **Releases** > **New release**).
    1. For **Tag name**, use the newly created tag.
    1. For **Release title**, just reuse the tag name.
    1. Check **Include message from the annotated tag**.
    1. Add link to newly created package to newly created release.
       1. For **URL** enter something similar to `https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/package_files/${package_id}/download`.
       1. For **Link title** enter the name of the package file.
    1. Click **Create Release**

## Updating base vscode version

### Goals

- Reproducible historic builds
  - Builds need to come from "tags" and release process includes tagging
- Intent of changes
  - Each individual commit will represent a cohesive, atomic change to the
    upstream behavior.
  - History of commits is really important!!
  - Let's include a CHANGELOG for every MR
- Reduce the cognitive load of knowing what changes we are making in our
  fork. This goal is _intentionally_ prioritized over preserving
  the historic state of commits on the fork, which is why we are
  preferring the `rebase ... -onto ...` paradigm over a merge-based
  paradigm. In other words, this fork is treated just like a feature
  branch. Don't fear the `rebase`.
- See some of the arguments made here for this philosophy:
  https://github.com/thewoolleyman/gitrflow/blob/master/readme/goals_and_philosophy.md
  - TL;DR previous versions of history don't matter, a clean current state
    with atomic commits showing cohesive intent is most important. Just like
    any other feature branch.

Downsides of this approach:

- Other than inspecting the history of previous tags, there's
  no way to see the previous versions of history by looking
  at the history of the main branch. But as described above,
  this is an intentional tradeoff to lower the cognitive load
  of understanding individual atomic changes.

### Process

TODO: expand with more details

- Find the newer upstream tag we want
- Open an MR
- Rebase the current fork commits onto the new tag
- Make any necessary changes, fully document them
- Interactive rebase changes into the relevant previous fork commits, if they
  are updates to the same changed behavior
- Push, get CI green, run tests, exploratory test
- Make sure that main branch has a tag of the latest commit
- force push the state of the MR to main
- rebase any open MR branches onto the new main

### Notes on Merge Requests

- The process for updating this repo against a new upstream tag involves rewriting/rebasing
  the history of main.
- This means that any MR which is open while the main branch is rebased will need
  to be rebased onto the new upstream.
- This will be a command like:

  ```
  # ensure `main` branch is pulled with latest
  git rebase (previous `git merge-base` SHA) --onto main
  ```
